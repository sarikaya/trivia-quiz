import React from "react";
import { Router, Scene, Stack, ActionConst } from "react-native-router-flux";

import Home from "./screens/Home";
import Question from "./screens/Question";
import Result from "./screens/Result";

export default class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Stack key="root">
          <Scene
            key="questionScreen"
            component={Question}
            animation="fade"
            hideNavBar={true}
            type={ActionConst.REPLACE}
          />
          <Scene
            key="resultScreen"
            component={Result}
            animation="fade"
            hideNavBar={true}
            type={ActionConst.REPLACE}
          />
          <Scene
            key="home"
            component={Home}
            animation="fade"
            hideNavBar={true}
            initial={true}
            type={ActionConst.REPLACE}
          />
        </Stack>
      </Router>
    );
  }
}
