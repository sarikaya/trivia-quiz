import React from "react";
import { StyleSheet, Animated, Text, View, Button } from "react-native";
import { Actions } from "react-native-router-flux";

export default class Home extends React.Component {
  constructor() {
    super();
    this.animatedValue = new Animated.Value(0);
  }
  componentDidMount() {
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 3000
    }).start();
  }
  onPress = () => {
    Actions.questionScreen();
  };
  render() {
    let color = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["#841584", "#fff"]
    });
    return (
      <Animated.View style={[styles.container, { backgroundColor: color }]}>
        <Text style={styles.title}>Trivia Quiz</Text>
        <Button onPress={this.onPress} title="Start Quiz!" color="#841584" />
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 48,
    fontWeight: "bold",
    color: "#841584"
  }
});
