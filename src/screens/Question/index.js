import React from "react";
import { StyleSheet, Text, View, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getQuestions,
  increaseScore,
  clearScore,
  saveTime
} from "../../redux/question/actions";
import QuestionModel from "../../components/QuestionModel";
import { Actions } from "react-native-router-flux";

class Question extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }
  async componentDidMount() {
    let { getQuestions, clearScore } = this.props;
    await getQuestions({
      amount: 10
    });
    clearScore();
    this.setState({
      loading: false
    });
  }
  onSubmit = time => {
    let { saveTime } = this.props;
    saveTime(time);
    Actions.resultScreen();
  };
  render() {
    let { questions, increaseScore } = this.props;
    let { loading } = this.state;
    return (
      <View style={styles.container}>
        {loading ? (
          <ActivityIndicator size="large" color="#841584" />
        ) : (
          <QuestionModel
            data={questions}
            start={Date.now()}
            onCorrectAnswer={() => increaseScore()}
            onSubmit={this.onSubmit}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});

const mapStateToProps = state => ({
  questions: state.question ? state.question.questions : []
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getQuestions,
      increaseScore,
      clearScore,
      saveTime
    },
    dispatch
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Question);
