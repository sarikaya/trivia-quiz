import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";

class Result extends React.Component {
  onPress = () => {
    Actions.questionScreen();
  };
  render() {
    let { score, questions } = this.props;
    let elapsed = Math.round(this.props.time / 100);
    let seconds = (elapsed / 10).toFixed(1);
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.text}>
            Total Score: {score} / {questions.length}
          </Text>
          <Text style={styles.text}>Time taken: {seconds} seconds</Text>
        </View>
        <Button onPress={this.onPress} title="Play Again" color="#841584" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "space-around"
  },
  text: {
    fontSize: 20
  }
});

const mapStateToProps = state => ({
  questions: state.question ? state.question.questions : [],
  score: state.question ? state.question.score : 0,
  time: state.question ? state.question.time : 0
});

export default connect(mapStateToProps)(Result);
