import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import he from "he";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";

export default class QuestionModel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      elapsed: 0,
      index: 0,
      answer: null,
      selectedIndex: null,
      buttonTitle: "Next",
      answerList: this.renderAnswers(props.data[0])
    };
  }
  componentDidMount() {
    this.timer = setInterval(this.tick, 50);
  }
  componentWillUnmout() {
    //Clear Interval before the component is removed
    clearInterval(this.timer);
  }
  tick = () => {
    let { start } = this.props;
    this.setState({
      elapsed: new Date() - start
    });
  };
  onPress = () => {
    let { index, selectedIndex, answer, elapsed } = this.state;
    let { data, onCorrectAnswer, onSubmit } = this.props;

    if (selectedIndex !== null) {
      //Correct Answer
      if (answer === 0) {
        onCorrectAnswer();
      }
      //Iterate for the next questions
      if (index < data.length - 1) {
        this.setState({
          index: index + 1,
          answerList: this.renderAnswers(data[index + 1])
        });
      }
      //Change submit button title for the last question
      if (index === data.length - 2) {
        this.setState({
          buttonTitle: "Submit Answers"
        });
      }
      //Submit all answers
      if (index === data.length - 1) {
        onSubmit(elapsed);
      }
    }
    this.setState({
      answer: null,
      selectedIndex: null
    });
  };

  onSelect = (index, value) => {
    this.setState({
      selectedIndex: index,
      answer: value
    });
  };

  renderAnswers = data => {
    // return answers with shuffle order
    return [
      <RadioButton value={0} key={0}>
        <Text style={styles.radioText}>{he.decode(data.correct_answer)}</Text>
      </RadioButton>,
      ...data.incorrect_answers.map((option, i) => (
        <RadioButton value={i + 1} key={i + 1}>
          <Text style={styles.radioText}>{he.decode(option)}</Text>
        </RadioButton>
      ))
    ].sort(() => Math.random() - 0.5);
  };

  render() {
    let { data } = this.props;
    let { index, selectedIndex, buttonTitle } = this.state;
    let elapsed = Math.round(this.state.elapsed / 100);
    let seconds = (elapsed / 10).toFixed(1);
    return (
      <View style={styles.container}>
        <View style={styles.top}>
          <Text>Time: {seconds}</Text>
          <Text>
            {index + 1} out of {data.length}
          </Text>
        </View>
        <Text style={styles.questionText}>
          {he.decode(data[index].question)}
        </Text>
        <View style={{ height: 50 }}>
          <RadioGroup onSelect={this.onSelect} selectedIndex={selectedIndex}>
            {this.state.answerList}
          </RadioGroup>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            disabled={selectedIndex === null}
            title={buttonTitle}
            color="#841584"
            onPress={this.onPress}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    margin: 24,
    backgroundColor: "#fff"
  },
  top: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 24,
    marginBottom: 24,
    fontSize: 16
  },
  questionText: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#841584"
  },
  radioText: {
    fontSize: 20
  },
  buttonContainer: {
    flex: 1,
    justifyContent: "flex-end"
  }
});
