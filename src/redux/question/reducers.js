import { actionType } from "./actions";
const initialState = {
  alert: null,
  questions: [],
  score: 0,
  time: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionType.GET_QUESTIONS:
      return {
        ...state,
        questions: action.payload,
        alert: null
      };
    case actionType.INCREASE_SCORE:
      return {
        ...state,
        score: state.score + 1,
        alert: null
      };
    case actionType.CLEAR_SCORE:
      return {
        ...state,
        score: 0,
        alert: null
      };
    case actionType.SAVE_TIME:
      return {
        ...state,
        time: action.payload,
        alert: null
      };
    case actionType.SERVER_ERR:
      return {
        ...state,
        alert: {
          message: action.payload.message,
          type: "err"
        }
      };
    default:
      return state;
  }
};
