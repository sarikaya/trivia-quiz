import axios from "axios";
import { baseUrl } from "../../constants/env";

export const actionType = {
  GET_QUESTIONS: "GET_QUESTIONS",
  INCREASE_SCORE: "INCREASE_SCORE",
  CLEAR_SCORE: "CLEAR_SCORE",
  SAVE_TIME: "SAVE_TIME",
  SERVER_ERR: "SERVER_ERR"
};

export const getQuestions = params => async dispatch => {
  try {
    const res = await axios.get(`${baseUrl}/api.php`, {
      params
    });
    dispatch({
      type: actionType.GET_QUESTIONS,
      payload: res.data.results
    });
  } catch (err) {
    dispatch({
      type: actionType.SERVER_ERR,
      payload: err.response
    });
  }
};

export const increaseScore = () => async dispatch => {
  dispatch({
    type: actionType.INCREASE_SCORE
  });
};

export const clearScore = () => async dispatch => {
  dispatch({
    type: actionType.CLEAR_SCORE
  });
};

export const saveTime = params => async dispatch => {
  dispatch({
    type: actionType.SAVE_TIME,
    payload: params
  });
};
