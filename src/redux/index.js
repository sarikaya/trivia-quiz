import { combineReducers } from "redux";
import question from "./question/reducers";

export default combineReducers({
  question
});
